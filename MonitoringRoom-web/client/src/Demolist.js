import React from 'react';

const Demolist = (props) => {
   return ( 
   <div className="Demostyle ma4 bg-light-purple dib pa3 grow shadow-4 tc">
        <img src={`https://joeschmoe.io/api/v1/${props.name}`} alt="Demo" />

        <h1 className=""> {props.name} </h1>
        

    </div>
   )
}
export default Demolist;