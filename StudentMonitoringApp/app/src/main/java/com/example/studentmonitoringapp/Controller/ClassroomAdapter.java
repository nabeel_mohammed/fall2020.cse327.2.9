package com.example.studentmonitoringapp.Controller;


        import android.annotation.SuppressLint;
        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;
        import androidx.annotation.NonNull;
        import androidx.recyclerview.widget.RecyclerView;
        import com.example.studentmonitoringapp.Model.ClassroomModel;
        import com.example.studentmonitoringapp.R;
        import com.google.firebase.auth.FirebaseAuth;
        import com.google.firebase.database.DatabaseReference;

        import java.util.ArrayList;


public class ClassRoomAdapter extends RecyclerView.Adapter<ClassRoomAdapter.MyViewHolder> {
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    Context context;
    ArrayList<ClassroomModel> model;

    public ClassRoomAdapter(Context c, ArrayList<ClassroomModel> m) {
        context = c;
        model = m;
    }

    @NonNull
    @Override
    public ClassRoomAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.post_list, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ClassRoomAdapter.MyViewHolder holder, final int pos) {
        holder.name.setText("☛ "+ model.get(pos).getName());
        holder.text.setText("☛ "+ model.get(pos).getText());

    }


    @Override
    public int getItemCount() {
        return model.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView name, text, time;
        public MyViewHolder(View itemView) {
            super(itemView);
            name =  itemView.findViewById(R.id.name);
            text =  itemView.findViewById(R.id.text);
        }

    }
}
